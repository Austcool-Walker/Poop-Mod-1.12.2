package tominator1.poop.common.Blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemBucket;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fluids.FluidUtil;
import tominator1.poop.common.PoopMod;
import tominator1.poop.common.Tiles.TileToilet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

import java.util.Random;

public class BlockToilet extends BlockContainer{

	public static final PropertyInteger FLUID_AMOUNT = PropertyInteger.create("fluid_amount", 0, 1500);

	public BlockToilet() {
		super(Material.IRON);
		this.setCreativeTab(PoopMod.tabShit);
		this.setHardness(5.0F);
		this.setResistance(10.0F);
		this.setSoundType(SoundType.METAL);
	}
	
	public TileEntity createNewTileEntity(World world, int par1){
		return new TileToilet();
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
		ItemStack current = player.inventory.getCurrentItem();
		TileEntity tile = world.getTileEntity(pos);
		if (tile instanceof TileToilet) {
			TileToilet tank = (TileToilet) tile;
			if (FluidUtil.getFluidContained(current) != null) {
				FluidStack liquid = FluidUtil.getFluidContained(current);
				if (liquid != null && liquid.getFluid() == FluidRegistry.WATER) {
					int qty = tank.fill(liquid, true);

					if (qty != 0 && !player.capabilities.isCreativeMode) {
						if (current.getCount() > 1) {
							if (!player.inventory.addItemStackToInventory(new ItemStack(new ItemBucket(tank.drain(liquid, true).getFluid().getBlock())))) {
								Random random = new Random();
								player.dropItem(new ItemStack(new ItemBucket(tank.drain(liquid, true).getFluid().getBlock()), random.nextInt(5) + 1), false);
							}

							player.inventory.setInventorySlotContents(player.inventory.currentItem,current.splitStack(1));
						} else {
							player.inventory.setInventorySlotContents(player.inventory.currentItem, current);
						}
					}
					world.setBlockState(pos, this.getDefaultState().withProperty(FLUID_AMOUNT, qty));
					return true;
				}
			}else if(current.getItem() == PoopMod.toiletPaper && tank.tankWater.getFluidAmount() >= 334){
				ItemStack var12 = new ItemStack(PoopMod.poopOnPaper, 1, 0);

				if (!player.inventory.addItemStackToInventory(var12))
				{
					world.spawnEntity(new EntityItem(world, (double)pos.getX() + 0.5D, (double)pos.getY() + 1.5D, (double)pos.getZ() + 0.5D, var12));
				}
				else if (player instanceof EntityPlayerMP)
				{
					((EntityPlayerMP)player).sendContainerToPlayer(player.inventoryContainer);
				}

				current.splitStack(1);

				if (current.getCount() <= 0)
				{
					player.inventory.setInventorySlotContents(player.inventory.currentItem, ItemStack.EMPTY);
				}
				tank.drain(333, true);
				world.setBlockState(pos, this.getDefaultState().withProperty(FLUID_AMOUNT, 0));
				return true;
			}
		}

		return false;
	}

	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, FLUID_AMOUNT);
	}
}