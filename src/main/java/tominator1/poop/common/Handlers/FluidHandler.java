package tominator1.poop.common.Handlers;

import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler;
import tominator1.poop.common.PoopMod;

import javax.annotation.Nullable;

public interface FluidHandler extends IFluidHandler{
    @Nullable
    @Override
    default FluidStack drain(FluidStack fluidStack, boolean b){
        if(b){
            return drain(fluidStack.amount--, true);
        }else{
            return drain(fluidStack.amount, true);
        }
    }

    @Nullable
    @Override
    default FluidStack drain(int i, boolean b){
        if(b){
            return drain(new FluidStack(PoopMod.liquidPoop, i), i--);
        }else{
            return drain(new FluidStack(PoopMod.liquidPoop, i), i);
        }
    }

    FluidStack drain(FluidStack fluid, int amount);
}
