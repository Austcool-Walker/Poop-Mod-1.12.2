package tominator1.poop.common.Packets;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import tominator1.poop.common.PoopMod;
import tominator1.poop.common.Tiles.TileIngotCaster;

import javax.annotation.Nonnull;

public class IngotCasterPacket implements IMessage {
	
	private int xCord;
	private int yCord;
	private int zCord;
	private int tankPoopAmount;
	private int tankPoopID;

	private int id = -1;

	public IngotCasterPacket(TileIngotCaster ingotCaster){
		if(ingotCaster.tankPoop.getFluid() == null) throw new RuntimeException("Cannot send packet of null.");
		xCord = ingotCaster.getPos().getX();
		yCord = ingotCaster.getPos().getY();
		zCord = ingotCaster.getPos().getZ();
		tankPoopAmount = ingotCaster.tankPoop.getFluidAmount();
		if(ingotCaster.tankPoop.getFluidAmount() == 0){
			ingotCaster.tankPoop.setFluid(new FluidStack(FluidRegistry.WATER, 0));
		}
		tankPoopID = id++;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		xCord = buf.readInt();
		yCord = buf.readInt();
		zCord = buf.readInt();
		tankPoopAmount = buf.readInt();
		tankPoopID = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(xCord);
		buf.writeInt(yCord);
		buf.writeInt(zCord);
		buf.writeInt(tankPoopAmount);
		buf.writeInt(tankPoopID);
	}

	public static class Handler implements IMessageHandler<IngotCasterPacket, IMessage> {

		@Override
		public IMessage onMessage(IngotCasterPacket message, MessageContext ctx) {
			EntityPlayer playerMP = PoopMod.proxy.getPlayerEntity(ctx);
			TileIngotCaster ingotCaster = (TileIngotCaster) playerMP.world.getTileEntity(new BlockPos(message.xCord, message.yCord, message.zCord));
			if(ingotCaster != null){
				ingotCaster.tankPoop.setFluid(new FluidStack(FluidRegistry.getFluid("poop"), message.tankPoopAmount));
			}
			return null;
		}
		
	}

}
