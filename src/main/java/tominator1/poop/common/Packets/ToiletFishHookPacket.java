package tominator1.poop.common.Packets;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import tominator1.poop.common.PoopMod;

public class ToiletFishHookPacket implements IMessage {
	
	private double xCord;
	private double yCord;
	private double zCord;
	private int fishHookID;

	public ToiletFishHookPacket(EntityFishHook fishHook){
		xCord = fishHook.posX;
		yCord = fishHook.posY;
		zCord = fishHook.posZ;
		fishHookID = fishHook.getEntityId();
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		xCord = buf.readDouble();
		yCord = buf.readDouble();
		zCord = buf.readDouble();
		fishHookID = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeDouble(xCord);
		buf.writeDouble(yCord);
		buf.writeDouble(zCord);
		buf.writeInt(fishHookID);
	}

	public static class Handler implements IMessageHandler<ToiletFishHookPacket, IMessage> {

		@Override
		public IMessage onMessage(ToiletFishHookPacket message, MessageContext ctx) {
			EntityPlayer playerMP = PoopMod.proxy.getPlayerEntity(ctx);
			EntityFishHook fishHook = (EntityFishHook) playerMP.world.getEntityByID(message.fishHookID);
			if(fishHook != null){
				fishHook.setPosition(message.xCord, message.yCord, message.zCord);	
			}
			return null;
		}
		
	}

}
