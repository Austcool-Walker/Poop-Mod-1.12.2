package tominator1.poop.common;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tominator1.poop.common.Blocks.BlockAutoToilet;
import tominator1.poop.common.Blocks.BlockIngotCaster;
import tominator1.poop.common.Blocks.BlockLiquidShit;
import tominator1.poop.common.Blocks.BlockPoop;
import tominator1.poop.common.Blocks.BlockToilet;
import tominator1.poop.common.Handlers.AutoToiletHandler;
import tominator1.poop.common.Handlers.BucketHandler;
import tominator1.poop.common.Handlers.CasterCraftingHandler;
import tominator1.poop.common.Handlers.FishingToiletHandler;
import tominator1.poop.common.Items.ItemPoopBucket;
import tominator1.poop.common.Items.ItemPoopGear;
import tominator1.poop.common.Items.ItemPoopIngot;
import tominator1.poop.common.Items.ItemPoopOnPaper;
import tominator1.poop.common.Items.ItemToiletPaper;
import tominator1.poop.common.Packets.AutoToiletPacket;
import tominator1.poop.common.Packets.IngotCasterPacket;
import tominator1.poop.common.Packets.ToiletFishHookPacket;
import tominator1.poop.common.Tiles.TileAutoToilet;
import tominator1.poop.common.Tiles.TileIngotCaster;
import tominator1.poop.common.Tiles.TileToilet;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

@Mod(modid = PoopMod.MOD_ID, name = PoopMod.MOD_NAME, version = PoopMod.VERSION)
public class PoopMod {
	
    public static final String MOD_ID = "poop";
    public static final String MOD_NAME = "Poop Mod";
    public static final String VERSION = "2.0";
	
	@Mod.Instance(MOD_ID)
	public static PoopMod instance;
	
	@SidedProxy(clientSide="tominator1.poop.client.poopClientProxy", serverSide="tominator1.poop.common.CommonProxy")
	public static CommonProxy proxy;
	
	public static SimpleNetworkWrapper network;
	public static Block poop;
	public static Item toiletPaper;
	public static Item poopOnPaper;
	public static Item poopIngot;
	public static Item poopGear;
	public static Fluid liquidPoop;
	public static Block liquidPoopBlock;
	public static Item poopBucket;
	public static Block toiletBlock;
	public static Block toiletAutoBlock;
	public static Block ingotCasterBlock;

	public static final Logger LOGGER = LogManager.getLogger(MOD_ID.toUpperCase());
	
	public static CreativeTabs tabShit= new CreativeTabs("shit") {
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(poop);
		}
	};
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
	    LOGGER.info("Starting pre-initialization phase of this shitty mod...");
	}

	@Mod.EventHandler
	public void load(FMLInitializationEvent event) {
		LOGGER.info("Starting initialization phase of this shitty mod...");
	}
}
