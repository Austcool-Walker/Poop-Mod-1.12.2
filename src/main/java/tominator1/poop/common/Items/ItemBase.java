package tominator1.poop.common.Items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBase extends Item {
    public ItemBase(String name) {
        this.setUnlocalizedName(name);
        this.setRegistryName(name);
    }

    public ItemBase(String name, CreativeTabs tab){
        this(name);
        this.setCreativeTab(tab);
    }

    public ItemBase(String name, int maxSize) {
        this(name);
        this.setMaxStackSize(maxSize);
    }

    public ItemBase(String name, int maxSize, CreativeTabs tab){
        this(name, maxSize);
        this.setCreativeTab(tab);
    }
}
