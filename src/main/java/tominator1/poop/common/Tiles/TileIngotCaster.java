package tominator1.poop.common.Tiles;

import net.minecraft.util.EnumFacing;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import tominator1.poop.common.PoopMod;
import tominator1.poop.common.Handlers.CasterCraftingHandler;
import tominator1.poop.common.Handlers.CasterCraftingHandler.RecipeOutputs;
import tominator1.poop.common.Packets.IngotCasterPacket;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;

import java.util.ArrayList;
import java.util.List;

public class TileIngotCaster extends TileEntity implements IFluidHandler, ISidedInventory{
	public FluidTank tankPoop = new FluidTank(Fluid.BUCKET_VOLUME*4);
	private ItemStack[] ingotCasterItemStacks = new ItemStack[1];
	private static final int[] SLOTS = new int[]{0};
	
	public boolean isUseableByPlayer(EntityPlayer player) {
		return world.getTileEntity(pos) == this &&
                player.getDistanceSq(this.pos.getX() + 0.5, this.pos.getY() + 0.5, this.pos.getZ() + 0.5) < 64;
	}
	
	public void updateEntity(){
		if(!world.isRemote){
		if (this.world.getTotalWorldTime() % 20L == 0L)
        {
			TileEntity tile3 = world.getTileEntity(pos);
			if(tile3 != null){
				if(tile3 instanceof IFluidHandler){
					int emptyness = tankPoop.getCapacity() - tankPoop.getFluidAmount();
					if(tankPoop.getFluid() == null) throw new NullPointerException("Fluids should not be null!");
					if(tankPoop.getFluidAmount() == 0 || tankPoop.getFluid().isFluidEqual(((IFluidHandler) tile3).drain(emptyness, false))){
						FluidStack drained = ((IFluidHandler) tile3).drain(emptyness, true);
						if(drained != null){
							tankPoop.setFluid(new FluidStack(drained.getFluid(), tankPoop.getFluidAmount()));
							tankPoop.fill(drained, true);
						}
					}
				}
			}
			RecipeOutputs Otputs = CasterCraftingHandler.INSTANCE.getRecipe(tankPoop.getFluid());
			if(Otputs != null){
				ItemStack copyItemStack = Otputs.itemStack.copy();
				FluidStack copyFluidStack = Otputs.fluidStack.copy();
				if(this.ingotCasterItemStacks[0] != null && this.ingotCasterItemStacks[0].isItemEqual(copyItemStack)){
					if(this.ingotCasterItemStacks[0].getCount() < getInventoryStackLimit()){
						this.ingotCasterItemStacks[0].setCount(copyItemStack.getCount());
						tankPoop.drain(copyFluidStack.amount, true);
					}
				}else if(this.ingotCasterItemStacks[0] == null){
					this.ingotCasterItemStacks[0] = copyItemStack;
					tankPoop.drain(copyFluidStack.amount, true);
				}
			}
			PoopMod.network.sendToAllAround(new IngotCasterPacket(this), new NetworkRegistry.TargetPoint(world.provider.getDimension(), this.pos.getX(), this.pos.getY(), this.pos.getZ(), 5));
        }
		}
	}
	
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound data)
	  {
		tankPoop.writeToNBT(data);
		NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.ingotCasterItemStacks.length; ++i)
        {
            if (this.ingotCasterItemStacks[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                this.ingotCasterItemStacks[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        data.setTag("Items", nbttaglist);
		return data;
	  }
	
	@Override
	public void readFromNBT(NBTTagCompound data)
	  {
		tankPoop.readFromNBT(data);
		NBTTagList nbttaglist = data.getTagList("Items", 10);
        this.ingotCasterItemStacks = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");

            if (b0 >= 0 && b0 < this.ingotCasterItemStacks.length)
            {
                this.ingotCasterItemStacks[b0] = new ItemStack(nbttagcompound1);
            }
        }
		super.readFromNBT(data);
	  }

	@Override
	public int fill(FluidStack resource2, boolean doFill){
		if (resource2 == null) {
			return 0;
		}
		FluidStack resourceCopy = resource2.copy();
		int totalUsed = 0;
		FluidTank tankToFill = tankPoop;
		if(tankPoop.getFluidAmount() == Fluid.BUCKET_VOLUME*4){
			return 0;
		}
		FluidStack liquid = tankToFill.getFluid();
		if (liquid != null && liquid.amount > 0 && !liquid.isFluidEqual(resourceCopy)) {
			return 0;
		}
		while (tankToFill.getFluid() != null && resourceCopy.amount > 0 && tankToFill.getFluidAmount() < Fluid.BUCKET_VOLUME*4) {
			int used = tankToFill.fill(resourceCopy, doFill);
			resourceCopy.amount -= used;
			
			if (used > 0) {
			}

			totalUsed += used;
		}
		return totalUsed;
	}

	@Override
	public FluidStack drain(int maxEmpty, boolean doDrain){
		if(doDrain) {
			return new FluidStack(PoopMod.liquidPoop, maxEmpty);
		}else{
			PoopMod.LOGGER.error("This API needs a makeover...jfc...this pisses me off >.< Better than anime tho *low key loves anime*");
			return null;
		}
	}

	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain) {
		if (resource == null || !resource.isFluidEqual(tankPoop.getFluid())) {
			return null;
		}
		return drain(resource.amount, doDrain);
	}

	@Override
	public IFluidTankProperties[] getTankProperties() {
		List<IFluidTankProperties> infos = new ArrayList<>();
		infos.add(new FluidTankProperties(tankPoop.getFluid(), tankPoop.getCapacity()));
		return (IFluidTankProperties[])infos.toArray();
	}

	@Override
	public int getSizeInventory() {
		return this.ingotCasterItemStacks.length;
	}

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
	public ItemStack getStackInSlot(int i) {
		return this.ingotCasterItemStacks[i];
	}

	@Override
	public ItemStack decrStackSize(int par1, int par2) {
		if (this.ingotCasterItemStacks[par1] != null)
        {
            ItemStack itemstack;

            if (this.ingotCasterItemStacks[par1].getCount() <= par2)
            {
                itemstack = this.ingotCasterItemStacks[par1];
                this.ingotCasterItemStacks[par1] = null;
                return itemstack;
            }
            else
            {
                itemstack = this.ingotCasterItemStacks[par1].splitStack(par2);

                if (this.ingotCasterItemStacks[par1].getCount() == 0)
                {
                    this.ingotCasterItemStacks[par1] = null;
                }

                return itemstack;
            }
        }
        else
        {
            return null;
        }
	}

    @Override
    public ItemStack removeStackFromSlot(int i) {
        return null;
    }

    @Override
	public void setInventorySlotContents(int par1, ItemStack par2) {
		
		this.ingotCasterItemStacks[par1] = par2;

        if (par2.getCount() > this.getInventoryStackLimit())
        {
        	par2.setCount(this.getInventoryStackLimit());
        }
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

    @Override
    public boolean isUsableByPlayer(EntityPlayer entityPlayer) {
        return false;
    }

    @Override
    public void openInventory(EntityPlayer entityPlayer) {

    }

    @Override
    public void closeInventory(EntityPlayer entityPlayer) {

    }

    @Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack) {
		return false;
	}

    @Override
    public int getField(int i) {
        return 0;
    }

    @Override
    public void setField(int i, int i1) {

    }

    @Override
    public int getFieldCount() {
        return 2;
    }

    @Override
    public void clear() {
        ingotCasterItemStacks[0] = ItemStack.EMPTY;
    }

    @Override
    public int[] getSlotsForFace(EnumFacing enumFacing) {
        return new int[0];
    }

    @Override
    public boolean canInsertItem(int i, ItemStack itemStack, EnumFacing enumFacing) {
        return true;
    }

    @Override
    public boolean canExtractItem(int i, ItemStack itemStack, EnumFacing enumFacing) {
        return true;
    }

    @Override
    public String getName() {
        return "container.ingotcaster";
    }

    @Override
    public boolean hasCustomName() {
        return true;
    }
}
