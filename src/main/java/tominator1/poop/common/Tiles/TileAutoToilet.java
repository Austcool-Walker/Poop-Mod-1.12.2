package tominator1.poop.common.Tiles;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.util.math.AxisAlignedBB;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import tominator1.poop.common.PoopMod;
import tominator1.poop.common.Blocks.BlockAutoToilet;
import tominator1.poop.common.Blocks.BlockToilet;
import tominator1.poop.common.Handlers.AutoToiletHandler;
import tominator1.poop.common.Packets.AutoToiletPacket;

import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fluids.FluidStack;

public class TileAutoToilet extends TileToilet{

	private BlockAutoToilet bloczek;
	public int mobsLength;
	
	public boolean isUseableByPlayer(EntityPlayer player) {
		return world.getTileEntity(pos) == this &&
                player.getDistanceSq(pos.getX() + 0.5, pos.getY()+ 0.5, pos.getZ() + 0.5) < 64;
	}
	
	public void updateEntity(){
		if(!world.isRemote){
            if (this.world.getTotalWorldTime() % 20L == 0L)
            {
                List mobs = this.world.getEntitiesWithinAABB(
                        EntityAnimal.class,
                        new AxisAlignedBB(pos)
                );
                int tempMobs = 0;
                for(int p = 0;p < mobs.size(); ++p){
                    if(AutoToiletHandler.INSTANCE.isThisMine((EntityAnimal) mobs.get(p), this)){
                        tempMobs++;
                    }
                }
                mobsLength = tempMobs;
                if(tankPoop.getFluidAmount() < 1000 && tankWater.getFluidAmount() > 0){
                    FluidStack i = tankWater.drain(mobsLength, true);
                    int j = i.amount;
                    tankPoop.fill(new FluidStack(PoopMod.liquidPoop, j), true);
                    world.setBlockState(pos, bloczek.getDefaultState().withProperty(BlockToilet.FLUID_AMOUNT, tankPoop.getFluidAmount()));
                }
                    PoopMod.network.sendToAllAround(new AutoToiletPacket(this), new NetworkRegistry.TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 10));
            }
		}
	}
	
	
	@Override
	public FluidStack drain(int maxEmpty, boolean doDrain){
		if(doDrain){
		    return new FluidStack(PoopMod.liquidPoop, maxEmpty);
        }
        return new FluidStack(PoopMod.liquidPoop, this.tankPoop.getFluidAmount());
	}

	@Override
	public FluidStack drain(FluidStack resource,boolean doDrain) {
		if (resource == null) {
			return null;
		}
		return drain(resource.amount, doDrain);
	}

    @Override
    public IFluidTankProperties[] getTankProperties() {
        List<IFluidTankProperties> infos = new ArrayList<>();
        infos.add(new FluidTankProperties(tankPoop.getFluid(), tankPoop.getCapacity()));
        return (IFluidTankProperties[])infos.toArray();
    }
	
}
