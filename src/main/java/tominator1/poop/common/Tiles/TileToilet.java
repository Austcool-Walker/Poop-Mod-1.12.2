package tominator1.poop.common.Tiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import tominator1.poop.common.PoopMod;
import tominator1.poop.common.Blocks.BlockToilet;
import tominator1.poop.common.Handlers.FishingToiletHandler;
import tominator1.poop.common.Packets.ToiletFishHookPacket;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.projectile.EntityFishHook;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;

public class TileToilet extends TileEntity implements IFluidHandler {
	private final Random rand = new Random();
	
	public FluidTank tankWater = new FluidTank(FluidRegistry.WATER, 0, Fluid.BUCKET_VOLUME);
	public FluidTank tankPoop = new FluidTank(PoopMod.liquidPoop, 0, Fluid.BUCKET_VOLUME);
	public EntityFishHook fishHook;
	private int fishHookTime;
	private boolean fishWasSucess = false;

	
	public void updateEntity(){
		if(!world.isRemote){
			WorldServer worldserver = (WorldServer)this.world;
			if (this.world.getTotalWorldTime() % 20L == 0L)
			{
				if(tankWater.getFluidAmount() >= 1000){
					List<EntityFishHook> hooks = this.world.getEntitiesWithinAABB(EntityFishHook.class, new AxisAlignedBB(this.pos.getX(), this.pos.getY()+1, this.pos.getZ(), this.pos.getX()+1, this.pos.getY()+2, this.pos.getZ()+1));
					for(int p = 0;p < hooks.size(); ++p){
						if(fishHook == null){
							fishHook = hooks.get(p);
							fishHook.setPosition(pos.getX()+0.5, pos.getY()+1, pos.getZ()+0.5);
							fishHookTime = 0;
							fishWasSucess = false;
							PoopMod.network.sendToAllAround(new ToiletFishHookPacket(fishHook), new NetworkRegistry.TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 10));
						}else if(fishHook == hooks.get(p)){
							fishHook.setPosition(pos.getX()+0.5, pos.getY()+1, pos.getZ()+0.5);
							PoopMod.network.sendToAllAround(new ToiletFishHookPacket(fishHook), new NetworkRegistry.TargetPoint(world.provider.getDimension(), pos.getX(), pos.getY(), pos.getZ(), 10));
						}else{
							EntityFishHook entityitem = hooks.get(p);
							float f = this.rand.nextFloat() * 0.8F + 0.1F;
							float f1 = this.rand.nextFloat() * 0.8F + 0.1F;
							float f2 = this.rand.nextFloat() * 0.8F + 0.1F;
							float f3 = 0.05F;
							entityitem.motionX = (double)((float)this.rand.nextGaussian() * f3);
							entityitem.motionY = (double)((float)this.rand.nextGaussian() * f3 + 0.2F);
							entityitem.motionZ = (double)((float)this.rand.nextGaussian() * f3);
						}
					}
					if(fishHook != null){
						if(fishHook.isDead){
							if(fishWasSucess){
								ItemStack itemekItemStack = FishingToiletHandler.INSTANCE.getRecipe();
								if(itemekItemStack != null){
									EntityItem entityitem = new EntityItem(this.world, this.pos.getX()+0.5, this.pos.getY()+1, this.pos.getZ()+0.5, itemekItemStack);
									this.world.spawnEntity(entityitem);
								}
							}
							fishHook = null;
						}else{
							fishHookTime++;
							worldserver.spawnParticle(EnumParticleTypes.WATER_SPLASH, fishHook.posX, fishHook.posY, fishHook.posZ, 2 + this.rand.nextInt(2), 0.10000000149011612D, 0.0D, 0.10000000149011612D, 0.0D);
							int randd = (int) (Math.random()*100);
							if(randd < fishHookTime){							
								fishHook.motionY -= 0.20000000298023224D;
								fishHook.playSound(SoundEvents.BLOCK_WATER_AMBIENT, 0.25F, 1.0F + (this.rand.nextFloat() - this.rand.nextFloat()) * 0.4F);
								float f1 = (float) MathHelper.floor(fishHook.getEntityBoundingBox().minY);
								worldserver.spawnParticle(EnumParticleTypes.WATER_BUBBLE, fishHook.posX, (double)(f1 + 1.0F), fishHook.posZ, (int)(1.0F + fishHook.width * 20.0F), (double)fishHook.width, 0.0D, (double)fishHook.width, 0.20000000298023224D);
								worldserver.spawnParticle(EnumParticleTypes.WATER_WAKE, fishHook.posX, (double)(f1 + 1.0F), fishHook.posZ, (int)(1.0F + fishHook.width * 20.0F), (double)fishHook.width, 0.0D, (double)fishHook.width, 0.20000000298023224D);
								fishHook.motionY -= (double)(this.rand.nextFloat() * this.rand.nextFloat() * this.rand.nextFloat()) * 0.2D;
								fishWasSucess = true;
							}
						}
					}
				}
			}
		}
	}
	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound)
	{
		compound.setInteger("tankWaterAmount", tankWater.getFluidAmount());
		compound.setInteger("tankPoopAmount", tankPoop.getFluidAmount());
		return compound;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound data)
	{
		tankWater.fill(new FluidStack(FluidRegistry.WATER, data.getInteger("tankWaterAmount")), true);
		tankPoop.fill(new FluidStack(PoopMod.liquidPoop, data.getInteger("tankPooopAmount")), true);
		super.readFromNBT(data);
  	}
	
	

	@Override
	public int fill(FluidStack stack, boolean doFill){
		BlockToilet toilet = null;
		if(world.getBlockState(pos).getBlock() instanceof BlockToilet) {
			toilet = (BlockToilet)world.getBlockState(pos).getBlock();
		}
		int totalUsed = 0;
		FluidTank tankToFill = tankWater;
		if(tankWater.getFluidAmount() == Fluid.BUCKET_VOLUME){
			return 0;
		}
		FluidStack liquid = tankToFill.getFluid();
		if (!stack.isFluidEqual(new FluidStack(FluidRegistry.WATER,1))) {
			return 0;
		}
		while (liquid != null && stack.amount > 0 && tankToFill.getFluidAmount() < Fluid.BUCKET_VOLUME) {
			int used = tankToFill.fill(stack, doFill);
			stack.amount -= used;
			
			if (used > 0) {
			}

			totalUsed += used;
		}
		if(toilet == null) return 0;
		world.setBlockState(pos, toilet.getDefaultState().withProperty(BlockToilet.FLUID_AMOUNT, tankToFill.getFluidAmount()));
		return totalUsed;
	}

	@Override
	public FluidStack drain(int amount, boolean doDrain){
		if(doDrain){
			return new FluidStack(PoopMod.liquidPoop, amount--);
		}else{
			PoopMod.LOGGER.info("doDrain = false //Honestly I don't know why there is a boolean for this");
			return new FluidStack(PoopMod.liquidPoop, amount);
		}
	}

	@Override
	public FluidStack drain(FluidStack resource, boolean doDrain) {
		if(resource == null){
			PoopMod.LOGGER.error("Fluid input does not exist!");
			return null;
		}else if (!resource.isFluidEqual(tankWater.getFluid())) {
			PoopMod.LOGGER.error("Could not drain " + resource.getUnlocalizedName() + "");
			return null;
		}
		return drain(resource.amount, doDrain);
	}

	@Override
	public IFluidTankProperties[] getTankProperties() {
		List<IFluidTankProperties> infos = new ArrayList<>();
		infos.add(new FluidTankProperties(this.tankPoop.getFluid(), this.tankPoop.getCapacity()));
		return (IFluidTankProperties[])infos.toArray();
	}
}
